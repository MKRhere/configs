#!/bin/bash
host="desktop.c0rn3j.com"
rsync -e "ssh -p 9595" ./fcrontab c0rn3j@$host:/tmp/fcrontab
ssh -p 9595 c0rn3j@$host fcrontab /tmp/fcrontab
rsync -e "ssh -p 9595" ./uploadPS3saves.sh c0rn3j@$host:/home/c0rn3j/
rsync -e "ssh -p 9595" ./smb.conf root@$host:/etc/samba/smb.conf
rsync -e "ssh -p 9595" ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -p 9595 root@$host "nginx -t && systemctl restart nginx"