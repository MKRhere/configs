#!/bin/bash
host="sc2.c0rn3j.com"
rsync -avz ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh root@$host "nginx -t && systemctl restart nginx"
rsync -avz ./fcrontab root@$host:/tmp/fcrontab
ssh root@$host fcrontab /tmp/fcrontab
rsync -avz ./updateGitWiki.sh root@$host:/root/
rsync -avz ./updateGitPrivatebin.sh root@$host:/root/
rsync -avz ./certrenew.sh root@$host:/root/
rsync -avz ./smokeping root@$host:/etc/