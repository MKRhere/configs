#! /bin/bash
# This script must have root rights to read the passwords

backupDate=$(date +%Y_%m_%d+%H-%M-%S)
filename="Backup-""$backupDate"
filepath=/home/c0rn3j/
fullpath=$filepath$filename
mysqldump --defaults-file=/home/c0rn3j/SQLcreds.txt --single-transaction --flush-logs --master-data=2 --all-databases > "$filepath"dbbackup.sql 
tar -cPf "$fullpath".tar "$filepath"dbbackup.sql
7z a "$fullpath".tar.7z "$fullpath".tar
gpg --batch --passphrase-file "$filepath"GPGcreds.txt -c "$fullpath".tar.7z 
umask 377
cp "$fullpath".tar.7z.gpg /mountables/backups/
rm "$filepath"dbbackup.sql "$fullpath".tar "$fullpath".tar.7z
