#!/bin/bash
host="sc3.c0rn3j.com"
rsync -avz ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh root@$host "nginx -t && systemctl restart nginx"
rsync -avz ./crontab root@$host:/tmp/crontab
ssh root@$host crontab /tmp/crontab
rsync -avz ./certrenew.sh root@$host:/root/
rsync -avz  ./ipdetect.php root@$host:/var/www/html/
rsync -avz  ./ipdetect.js root@$host:/var/www/html/
rsync -avz  ./tradechecker root@$host:/var/www/html/
rsync -avz  ./mcskins root@$host:/var/www/html/
