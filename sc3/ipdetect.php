<?php
// This script is AGPLv3 licensed.
function ipVersion($txt) {
     return strpos($txt, ":") === false ? 4 : 6;
}
function getOS() {
    global $user_agent;
    $os_platform    =   "Unknown OS Platform";
    $os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}

function getBrowser() {
    global $user_agent;
    $browser        =   "Unknown Browser";
    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/trident/i'    =>  'Internet Explorer(trident)',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser',
                            '/vivaldi/i'    =>  'Vivaldi'
                        );
    foreach ($browser_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }
    }
    return $browser;
}

//If we're on the ip subdomain and not the ipv4 or ipv6 one, do everything
if($_SERVER['HTTP_HOST'] == "ip.c0rn3j.com")
{
    printf("<strong>Your IPv4:</strong> <span id=\"main\"></span><br>");
    printf("<strong>Your IPv6:</strong> <span id=\"subby\"></span><br>");
    printf("<strong>Your local IPs are(you need a modern browser that supports WebRTC for this one to work):</strong><br> <span id=list>-</span><br>");
    $user_agent     =   $_SERVER['HTTP_USER_AGENT'];
    $user_os        =   getOS();
    $user_browser   =   getBrowser();
    $device_details =   "<strong>Operating System: </strong>".$user_os."<br><strong>Browser: </strong>".$user_browser;
    printf($device_details);
    
    echo("<br><strong>User agent: </strong>".$_SERVER['HTTP_USER_AGENT']."<br>");
    
    $serverProtocol = $_SERVER['SERVER_PROTOCOL'];
    printf("<strong>You are connected via: </strong>".$serverProtocol);
    
    echo("<br><br><a href=\"https://gitlab.com/C0rn3j/configs/tree/master/sc3\">Hey, this script is open-source under the AGPLv3 license!</a>");
    //Get IPv4 and IPv6 via AJAX
    echo("
<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>
<script>
function getIP(subdomain) {
    return $.get(\"https://\"+ subdomain +\".c0rn3j.com\");
  }
  
  getIP(\"ipv4\").then(res => $(\"#main\").text(res));
  getIP(\"ipv6\").then(res => $(\"#subby\").text(res));
  
  </script>
  ");  
  //Get all local adresses on both IPv4 and IPv6 via WebRTC
  //Why/how this works https://www.whatismybrowser.com/detect/what-is-my-local-ip-address
  echo('<script type="application/javascript" src="/ipdetect.js"></script>');
}
//Else if current site is not on the ip subdomain it is ipv4 or ipv6, so just show the IP. This is also very curl friendly.
else
{
    printf($_SERVER['REMOTE_ADDR']);
}
?>

